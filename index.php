<?php
	require('db.php');
	$msg='';

	if(isset($_GET['r']) && $_GET['r']=="s"){
		$msg="<strong>Your Session may expire </strong>! <br>Please Login";
	}

	if(isset($_GET['r']) && $_GET['r'] == 'logout'){
		$msg = '<strong>Logout Successful !</strong>';
	}

	if(isset($_POST['log'])){

		$email = $_POST['email'];
		$pass  = $_POST['pass'];

		$log = mysqli_query($con, "select * from user where email='$email' and pass='$pass'");

		$amt = mysqli_affected_rows($con);
		$rows = mysqli_fetch_array($log, MYSQLI_ASSOC);

		if($email=="" || $pass==""){
			$msg="<strong>Input Missing!</strong> <br>Please retry";
		}
		
		else if($amt==0){
			$msg="<strong>Wrong user id and password!</strong> <br>Please retry";
		}
		
		else if($rows["status"]==0){
			$msg="<strong>Your account is not activated.</strong><br> Please contact with Admin.";
		}
		
		else if($rows["status"]==2){
			$msg="<strong>Your account has been blocked.</strong><br> Please contact with admin.";
		}
		else{
		session_start();
		$_SESSION['id'] = $rows['id'];
		$_SESSION['name'] = $rows['name'];
		$_SESSION['email'] = $rows['email'];
		
		header("Location: dashboard.php");
	}
	}

?>
<html style="height:100%">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Quantam Cloud</title>	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>
<body>
	<div class="login-container material">

		<!-- Page content -->
		<div class="page-content">

			<!-- Simple login form -->
			<form action="" method="POST">			
				<div class="col-sm-4 col-sm-offset-4">							
					<div class="welcome bg-blue p-t-20">						
						<div class="text-center m-b-20">
						<img src="img/s-megastore-logo.jpg" class="img-responsive img-circle max-width-100" alt="">						
						<h2>MEGA<b>STORE</b></h2>
						<h5>Shop Inventory Management System</h5>
					</div>
					</div>
					<div class="panel panel-flat no-border">

						<div class="panel-body no-padding-bottom">
						
						<?php if($msg!= NULL) { ?>
						<div class="alert alert-danger alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin-right:20px;"><span aria-hidden="true">&times;</span></button>
						   <?php echo $msg; ?>
						</div>
						<?php } ?>
						
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Email" name="email" value="guest@gmail.com" required="required">							
							</div>

							<div class="form-group">
								<input type="password" class="form-control" placeholder="Password" name="pass" value="123456" required="required">							
							</div>

							<div class="login-options">
								<div class="row">
									<div class="col-sm-6 col-xs-6">
										<div class="checkbox">
											<label>
												<input type="checkbox" class="styled" checked="checked">
												Remember me
											</label>
										</div>
									</div>

									<div class="col-sm-6 col-xs-6 text-right">
										<button type="submit" class="btn bg-blue no-border-radius" name="log">Login</button>																		
									</div>
								</div>
							</div>
							<div class="panel-footer">
							<a href="signup.php">Create a New Accounts</a>
						</div>
							
						</div>
						
					</div>
				</div>
				
			</form>
			<!-- /simple login form -->

		</div>
		<!-- /page content -->

	</div>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>


</html>