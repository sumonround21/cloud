<?php session_start();
		if($_SESSION['id']=="" || $_SESSION['name']=="" || $_SESSION['email']=="") header("Location: index.php?r=s"); 
		require_once "db.php";
		
		$u_id=$_SESSION['id'];
		 
$dash=mysqli_query($con,"select * from user where id=$u_id");
$row=mysqli_fetch_array($dash,MYSQLI_ASSOC);

$pro = mysqli_query($con, "select * from products");

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      <li><a href="#">Page 1</a></li>
      <li><a href="#">Page 2</a></li>
    </ul>
     <form class="navbar-form navbar-left" action="search.php" method="GET">
      <div class="form-group">
        <input type="text" class="form-control" name="search" placeholder="Search">
      </div>
      <button type="submit" name="submit" class="btn btn-default">Search</button>
    </form>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#"><span class="glyphicon glyphicon-user"></span> <?php echo $row['name'];  ?></a></li>
      <li><a href="logout.php" ><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
    </ul>
  </div>
</nav>
<div class="container">
  <h1>My First Bootstrap Page</h1>
  	<a href="product.php" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add New Products</a><br><br>
   <table class="table table-bordered table-condensed">
    <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Password</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <?php 
    	if(count($pro)> 0){
    	while($re=mysqli_fetch_array($pro)){ 
    ?>
      <tr>
        <td><?php echo $re['title']; ?></td>
        <td><?php echo $re['price']; ?></td>
        <td><?php echo $re['description']; ?></td>
        <td><a href="edit.php?id=<?php echo $re['id']; ?>" class="btn btn-primary btn-sm">Edit</a> || <a href="del.php?id=<?php echo $re['id']; ?>&table=products&field=id" class="btn btn-primary btn-sm">Del</a></td>
      </tr>
     <?php } }else { echo "Data Not Found !"; } ?>
    </tbody>
  </table>
</div>

</body>
</html>

