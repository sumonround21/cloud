<?php 
  session_start();
  $msg='';
		if($_SESSION['id']=="" || $_SESSION['name']=="" || $_SESSION['email']=="") header("Location: index.php?r=s"); 
		require_once "db.php";
		
		$u_id=$_SESSION['id'];
		 
if(isset($_POST['insert'])){
    $title = mysqli_real_escape_string($con, trim($_POST['title']));
    $quantity = mysqli_real_escape_string($con, trim($_POST['quantity']));
    $price = mysqli_real_escape_string($con, trim($_POST['price']));
    $description = mysqli_real_escape_string($con, trim($_POST['description']));

    

    if($title == "" || $quantity=="" || $price==""){
      $msg="<strong>Input Missing!</strong> <br>Please retry";
    }else{

      mysqli_query($con,"insert into products(`title`,`user_id`, `quantity`,`price`,`description`) values('$title','$u_id','$quantity', '$price', '$description')");
  

      $msg="<strong>Product Successfully Submitted !</strong>!";
    }
    
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      <li><a href="#">Page 1</a></li>
      <li><a href="#">Page 2</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#"><span class="glyphicon glyphicon-user"></span> Signup</a></li>
      <li><a href="logout.php" ><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
    </ul>
  </div>
</nav>
<div class="container">
  <div class="col-sm-6 col-sm-offset-3">
            <?php if($msg!= NULL) { ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin-right:20px;"><span aria-hidden="true">&times;</span></button>
               <?php echo $msg; ?>
            </div>
            <?php } ?>
          <form action="" method="post">
            <div class="form-group">
              <label for="title">Title:</label>
              <input type="text" class="form-control" id="title" name="title">
            </div>
            <div class="form-group">
              <label for="quantity">Quantity:</label>
              <input type="text" class="form-control" id="quantity" name="quantity">
            </div>
            <div class="form-group">
              <label for="price">Price:</label>
              <input type="text" class="form-control" id="price" name="price">
            </div>
            <div class="form-group">
              <label for="description">Description:</label>
              <input type="text" class="form-control" id="description" name="description">
            </div>
           
            <button type="submit" class="btn btn-default" name="insert">Submit</button>
          </form>
        </div>
      </div>

</body>
</html>

