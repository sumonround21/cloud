<?php
	require('db.php');
	$msg='';

	if(isset($_POST['log'])){
		$name=mysqli_real_escape_string($con, trim($_POST['name']));
		$email=mysqli_real_escape_string($con, trim($_POST['email']));
		$pass=mysqli_real_escape_string($con, trim($_POST['pass']));

		$log = mysqli_query($con, "select * from user where email='$email' and pass='$pass'");

		if($name == "" || $email=="" || $pass==""){
			$msg="<strong>Input Missing!</strong> <br>Please retry";
		}else{

			mysqli_query($con,"insert into user(`name`,`email`,`pass`) values('$name','$email','$pass')");
	

			$msg="<strong>Account Successfuly Created !</strong>!";
		}
		
		$to      = $email; // Send email to our user
		$subject = 'Signup | Verification'; // Give the email a subject 
		$message = '
		 
		Thanks for signing up!
		Your account has been created, you can login with the following credentials after you have activated your account by pressing the url below.
		 
		------------------------
		Username: '.$name.'
		Password: '.$pass.'
		------------------------
		 
		Please click this link to activate your account:
		http://localhost/cloud/verify.php?email='.$email.'&hash='.$hash;
		$headers = 'From:noreply@yourwebsite.com' . "\r\n"; // Set from headers
		mail($to, $subject, $message, $headers); // Send our email





	}
	

?>
<html style="height:100%">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Quantam Cloud</title>	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>
<body>
	<div class="login-container material">

		<!-- Page content -->
		<div class="page-content">

			<!-- Simple login form -->
			<form action="" method="POST">			
				<div class="col-sm-4 col-sm-offset-4">							
					<div class="welcome bg-blue p-t-20">						
						<div class="text-center m-b-20">
						<img src="img/s-megastore-logo.jpg" class="img-responsive img-circle max-width-100" alt="">						
						<h2>MEGA<b>STORE</b></h2>
						<h5>Shop Inventory Management System</h5>
					</div>
					</div>
					<div class="panel panel-flat no-border">

						<div class="panel-body no-padding-bottom">
						
						<?php if($msg!= NULL) { ?>
						<div class="alert alert-danger alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin-right:20px;"><span aria-hidden="true">&times;</span></button>
						   <?php echo $msg; ?>
						</div>
						<?php } ?>
						
							<div class="form-group">
								<input type="text" class="form-control" placeholder="User Name" name="name"  >						
							</div>
						
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Email" name="email">							
							</div>
					

							<div class="form-group">
								<input type="password" class="form-control" placeholder="Password" name="pass" >							
							</div>

							<div class="login-options">
								<div class="row">
									<div class="col-sm-12 col-xs-12 text-right">
										<button type="submit" class="btn bg-blue no-border-radius" name="log">Signup</button>																		
									</div>
								</div>
							</div>
							<div class="panel-footer">
							<a href="index.php">Sign in</a>
						</div>
							
						</div>
						
					</div>
				</div>
				
			</form>
			<!-- /simple login form -->

		</div>
		<!-- /page content -->

	</div>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>


</html>